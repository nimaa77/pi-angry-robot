var io = require('socket.io-client');
var socket = io.connect('http://136.243.118.59:8080', {reconnect: true});
var Gpio = require('pigpio').Gpio,
	trigger = new Gpio(23, {mode: Gpio.OUTPUT}),
	echo = new Gpio(24, {mode: Gpio.INPUT, alert: true}),
	PWMA1 = new Gpio(6, {mode: Gpio.OUTPUT}),
	PWMA2 = new Gpio(13, {mode: Gpio.OUTPUT}),
	PWMB1 = new Gpio(20, {mode: Gpio.OUTPUT}),
	PWMB2 = new Gpio(21, {mode: Gpio.OUTPUT}),
	D1 = new Gpio(12, {mode: Gpio.OUTPUT}),
	D2 = new Gpio(26, {mode: Gpio.OUTPUT}),
	dutyCycle = 100;

function init()
{
	const MICROSECDONDS_PER_CM = 1e6/34321;

	(function () {
		var startTick;
	  
		echo.on('alert', function (level, tick) {
		  var endTick, diff;
		  if (level == 1) {
			  startTick = tick;
		  } else {
			  endTick = tick;
			  diff = (endTick >> 0) - (startTick >> 0);
			  console.log("Distance Is: ", diff / 0.000058);
			  socket.emit('distance' , diff / 0.000058);
		  }
		});
	}());

	setInterval(function () {
	  trigger.trigger(10, 1);
	  console.log("hey");
	}, 1000);

	D1.pwmFrequency(500);
	D2.pwmFrequency(500);

	D1.pwmWrite(dutyCycle);
	D2.pwmWrite(dutyCycle);

	trigger.digitalWrite(0);
}

function left()
{
	set_motor(1,0,0,0);
}

function right()
{
	set_motor(0,0,1,0);
}

function forward()
{
	set_motor(1,0,1,0);
}

function reverse()
{
	set_motor(0,1,0,1);
}

function stop()
{
	set_motor(0,0,0,0);
}

function set_motor(A1,A2,B1,B2)
{
	PWMA1.digitalWrite(A1);
	PWMA2.digitalWrite(A2);
	PWMB1.digitalWrite(B1);
	PWMB2.digitalWrite(B2);
}

function speedUp()
{
	if (dutyCycle + 10 < 101) {
		dutyCycle = dutyCycle + 10;
		D1.pwmWrite(dutyCycle);
		D2.pwmWrite(dutyCycle);
	}
}

function speedDown()
{
	if (dutyCycle - 10 > -1) {
		dutyCycle = dutyCycle - 10;
		D1.pwmWrite(dutyCycle);
		D2.pwmWrite(dutyCycle);
	}
}

socket.on('connect', function (socket) {
	console.log("Connected To Server");
	init();
	stop();
});

socket.on('right', function(codeUrl){
	console.log("Going Right");
	right();
});

socket.on('left', function(codeUrl){
	console.log("Going Left");
	left();
});

socket.on('str', function(codeUrl){
	console.log("Going Forward");
	forward();
});

socket.on('bkw', function(codeUrl){
	console.log("Going Backward");
	reverse();
});

socket.on('stop', function(codeUrl){
	console.log("Stopped");
	stop();
});

socket.on('speedup', function(codeUrl){
	console.log("Speed Up");
	speedUp();
});

socket.on('speeddown', function(codeUrl){
	console.log("Speed Down");
	speedDown();
});
